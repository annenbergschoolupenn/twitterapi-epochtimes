#!/bin/bash
# epj@asc.upenn.edu

# build timeline getter
docker build -t timeline_getter -f ./Dockerfile .

# run timeline getter
docker run -it --network=host -v /Users/etiennejacquot/Documents/Bitbucket/twitterapi-epochtimes/data:/app/data/ timeline_getter

# remove the previous container to keep things clean, we'll reuse this container name each time
docker rm timeline_getter