FROM python:3.8-slim

COPY ./configs /app/configs

WORKDIR /app/

RUN pip install -r ./configs/requirements.txt

COPY ./TimelineGetter.py /app/TimelineGetter.py

CMD "python" "TimelineGetter.py" "epochtimes"