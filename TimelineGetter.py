import tweepy
import configparser
import os
import json
import sys
import datetime
from time import strptime
import pandas as pd
import pyarrow
#from collections import Counter

# twitter access tokens to configs/config.ini file!
twitter_cred = {}
config = configparser.ConfigParser()
config.read("/app/configs/config.ini")
#config.read('../twitterapi-epochtimes/configs/config.ini') # <--- add your Twitter API tokens to this file!
for item,value in config['TWITTER'].items():
    twitter_cred[item]=value

# Setup Twitter Authentication
auth = tweepy.OAuthHandler(twitter_cred['consumer_key'], twitter_cred['consumer_secret'])
auth.set_access_token(twitter_cred['access_key'], twitter_cred['access_secret'])

# Create Twitter API object
api = tweepy.API(auth, 
                 wait_on_rate_limit=True, 
                 wait_on_rate_limit_notify=True)
                 #,parser=tweepy.parsers.JSONParser())

# Test API authentication
try:
    api.verify_credentials()
    print("Authentication OK!")
except:
    print("Error during authentication...")

# get twitter username from Dockerfile argument
twitter_username = sys.argv[1]
print('Getting tweets for {}'.format(twitter_username))

# Check for user outdir
if not os.path.exists('/app/data/timeline_{}'.format(twitter_username.strip('@'))):
    os.makedirs('/app/data/timeline_{}'.format(twitter_username.strip('@')))
    os.makedirs('/app/data/timeline_{}/raw'.format(twitter_username.strip('@')))

##################################
# REALLY THIS SHOULD BE FORMAT:
##################################
# data = []
# for item in tweepy.Cursor:
#     data.append(item) 
# pd.concat(data)
# Prepare df for cursing through twitter user timeline
##################################
# This also isn't technically the 'raw' output, given we are only appending `status._json`... 
# I forget what other metadata in the raw I am excluding w/ this ... 
twitter_timeline_df = pd.DataFrame()
for status in tweepy.Cursor(api.user_timeline, user_id=api.get_user(twitter_username).id, tweet_mode="extended").items():
    twitter_timeline_df = twitter_timeline_df.append(status._json,ignore_index=True)

print('Done! --> Number of tweets returned for {} is: {}'.format(twitter_username,twitter_timeline_df.shape[0]))

# Preparing json export of timeline df
filename = twitter_username.strip('@')+"_"+str(datetime.date.today())+".json"    
outfile = '/app/data/timeline_{}/raw/{}'.format(twitter_username.strip('@'),filename)

# Exporting to json
twitter_timeline_df.to_json(outfile)
print('Exported to --> {}'.format(outfile))
print('-'*50)