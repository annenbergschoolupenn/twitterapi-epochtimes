# EpochTimes Twitter Request

Etienne Jacquot - epj@asc.upenn.edu 01/31/21

_____________
## AWS Athena for Historical Results

- It turns out that Tian is much more interested in temporal data, so the docker work original shared for this project was maybe not the most useful (remember our API calls only return most recent 3,200 tweets)...

- *AWS Athena:*Queried the data and provided two CSV exports, this is around **1,001** tweets from `@epochtimes` in our ASC Wharton historical database. This goes back to around 2015 I think... Somewhat impressive that this account made it into the daily 1% random sampling at least 1,000 times!


- Using twitterAPIv2 we can get engagement for those tweet_ids, please see notebook example in [apiv2](./apiv2/)
	- You need a Twitter apiv2 `BEARER_TOKEN`... for the sake of testing I have hardcoded this into the notebook!)
	- so far I have results for like 300 tweets, this hit the rate limit on testing ...
_____________
## Getting Started
### Twitter api keys

Get your twitter credentials online w/ twitter developer https://developer.twitter.com/en/portal/dashboard

- Save these in your `config.ini` file, and don't share! We add `*.ini` to the *.gitignore* file to avoid publishing these to Bitbucket ...

### Set twitter username for api call

Edit the `Dockerfile` to container your Twitter username of choice, in this case **epochtimes** https://twitter.com/epochtimes

- This assumes you have Docker & Docker-compose installed on your system... more info [here](https://docs.docker.com/get-docker/)

``` diff
- CMD "python" "TimelineGetter.py" "TWITTER_USERNAME"
+ CMD "python" "TimelineGetter.py" "epochtimes"
```
_________

## Twitter API + Docker

The [deploy.sh](./deploy.sh) script has commands available

### Build your container for EpochTimes *timelineGetter*

- To build your docker container:

``` bash
#!/bin/bash
# build timeline getter
docker build -t timeline_getter -f ./Dockerfile .
```

- to run your docker container, the following will write json output to `./data` as your volume mount

``` bash
# run timeline getter
docker run -it --network=host -v /Users/etiennejacquot/Documents/Bitbucket/twitterapi-epochtimes/data/:/app/data/ timeline_getter
```

#### *RESULT OUTPUT FROM DOCKER CONTAINER*

- for example run on 01/25/2021 @ 10:45PM~:

```                                 
Authentication OK!
Getting tweets for epochtimes
Done! --> Number of tweets returned for epochtimes is: 3213
Exported to --> /app/data/timeline_epochtimes/raw/epochtimes_2021-01-26.json
--------------------------------------------------
```

This returned **3213** tweets on the timeline, but is this all tweets?

- there are other API calls you can run to get the *total # of tweets* for that account... But to actually get all the tweets, I think this is using pagination already, so might be the maximum for twitter_apiv1 timelineGetter... Twitter_apiv2 probably has a solution for this

______

## ETL for AWS Athena Access

Now we clean with etl notebook & prepare SQL syntax for Athena table creation

### Cleaning up data:

--> **Simplified TwitterAPI JSON result ETL cleanup** found here: [twitter_data_etl-simplified.ipynb](./twitter_data_etl-simplified.ipynb)
- you can share the out json or upload parquet to S3, simplified is *not* on AWS athena...
- These etl results are also helpful for the `statelvl_publichealth` twitter project!


For more involved cleanup (specifically to try match wharton twitter df 60 columns) ETL cleanup workflow is here:[twitter_data_etl.ipynb](./twitter_data_etl.ipynb) 
- as of 01/25/26 I have a sample sql syntax for table creation that matches the parquet dtypes...
- Contains SQL query example as well, need to confirm if the dtypes get passed correctly to Athena
- Access twitter day on https://aws.cloud.upenn.edu, *still in progress* ... 

Make sure to create your S3 bucket on AWS
- In this case, `s3://asctwitterdata/epochtimes/`
    
_____________

## TODO!
- reach out to end user
    - how do you want to access data? json, csv cleaned, or athena for query?
    - raw, etl simplified, or wharton df parity?
- add end user to BitBucket repo for code collaboration
    - create a repo for this
- add end user to AWS for Athena query

Twitter API v2 has info on timeline, see here: https://developer.twitter.com/en/docs/twitter-api/tweets/timelines/introduction

- this mentions the most recent 3,200 are returned to the endpoint. This only mentions `start_time` and `end_time`, can we go back to 2009 when the `@epochtimes` account was created to then paginate through old files?
    
- I haven't tried using docker-compose for this process... there is a sample [docker-compose.yml](./docker-compose.yml) for testing ... maybe the etl could run as a service *after* the twitter_api

